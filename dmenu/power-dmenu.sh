#!/bin/bash

function powermenu {
    options="sleep\nshutdown\nreboot\nlogout\ncancel"
    dmenu_command="dmenu -c -l 2"
    selected=$(echo -e $options | dmenu -c -l 5 -m 0 -p powermenu)
    if [[ $selected == "shutdown" ]]; then
        confirm=$(echo -e "Yes, $selected\nNo, cancel" | $dmenu_command -p "Are you sure?")
        if [[ $confirm == "Yes, $selected" ]]; then
            systemctl poweroff
        else
            return
        fi
    elif [[ $selected == "reboot" ]]; then
        confirm=$(echo -e "Yes, $selected\nNo, cancel" | $dmenu_command -p "Are you sure?")
        if [[ $confirm == "Yes, $selected" ]]; then
            systemctl reboot
        else
            return
        fi
    elif [[ $selected == "sleep" ]]; then
        confirm=$(echo -e "Hibernate\nSuspend" | $dmenu_command -p "Chose sleep method")
        if [[ $confirm == "Hibernate" ]]; then
            systemctl hibernate
        elif [[ $confirm == "Suspend" ]]; then
            systemctl suspend
        else
            return
        fi
    elif [[ $selected == "logout" ]]; then
        loginctl terminate-session ${XDG_SESSION_ID-}
    elif [[ $selected == "cancel" ]]; then
        return
    fi
}
powermenu
