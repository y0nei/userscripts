#!/bin/bash

# Emojis sourced from https://github.com/git-list/emoji-list/blob/master/README.md
cat ~/userscripts/dmenu/emojis | dmenu -l 8 -c -h 34 | awk '{print $1}' | tr -d '\n' | xclip -selection clipboard

notify-send "$(xclip -o -selection clipboard) copied to clipboard."