#!/bin/bash

if pgrep picom >/dev/null; then
    killall picom &
    notify-send "picom killed"
else
    picom --experimental-backends -b &
    notify-send "picom launched"
fi